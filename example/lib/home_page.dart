import 'package:flutter/material.dart';
import 'package:pomodoro_flutter/pomodoro.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _configuration = PomodoroConfiguration(
    workingMinutes: 1,
    totalPomodori: 3,
    breakMinutes: 1,
    countUntilLongerBreak: 2,
    longerBreakMinutes: 4,
    autoStart: false,
  );

  int totalPomodoro = 0;
  PomodoroStatus? _pomodoroStatus;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Pomodoro Example"),
      ),
      body: Column(
        children: [
          const SizedBox(height: 20),
          Pomodoro(
            configuration: _configuration,
            onPomodoroWorkFinished: () {
              setState(() => totalPomodoro++);
            },
            onAllPomodoriCompleted: showCompleted,
            onStatusChanged: (status) {
              debugPrint("Status: $status");
              setState(() => _pomodoroStatus = status);
            },
          ),
          _pomodoroStatusWidget(_pomodoroStatus),
          Card(
            child: ListTile(
              title: const Text("Sample task"),
              subtitle: const Text("This is sample task description"),
              trailing: Text("$totalPomodoro"),
            ),
          )
        ],
      ),
    );
  }

  void showCompleted() {
    var bar = const SnackBar(content: Text("All completed"));
    ScaffoldMessenger.of(context).showSnackBar(bar);
  }

  Widget _pomodoroStatusWidget(PomodoroStatus? status) {
    if(status == null) return const SizedBox();

    return Text(
      status.name,
      style: Theme.of(context).textTheme.titleLarge,
    );
  }
}
