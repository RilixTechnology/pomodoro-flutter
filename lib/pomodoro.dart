import 'package:flutter/material.dart';
import 'src/pomodoro_status.dart';
import 'src/pomodoro_configuration.dart';
import 'src/pomodoro_view.dart';
export 'src/pomodoro_status.dart';
export 'src/pomodoro_configuration.dart';

class Pomodoro extends StatelessWidget {
  final PomodoroConfiguration? configuration;
  final VoidCallback? onPomodoroWorkStarted;
  /// onPomodoroWorkFinished callback when single pomodoro is finished.
  final VoidCallback? onPomodoroWorkFinished;
  final VoidCallback? onPomodoroBreakStart;
  final VoidCallback? onPomodoroBreakEnd;
  final VoidCallback? onAllPomodoriCompleted;
  final VoidCallback? onPomodoroLongBreakStart;
  final VoidCallback? onPomodoroLongBreakEnd;
  final VoidCallback? onCompleted;
  final VoidCallback? onStopped;
  final Function(PomodoroStatus status)? onStatusChanged;
  final double radius;

  const Pomodoro({
    super.key,
    this.configuration,
    this.onPomodoroWorkStarted,
    this.onPomodoroWorkFinished,
    this.onPomodoroBreakStart,
    this.onPomodoroBreakEnd,
    this.onAllPomodoriCompleted,
    this.onPomodoroLongBreakStart,
    this.onPomodoroLongBreakEnd,
    this.onCompleted,
    this.onStopped,
    this.onStatusChanged,
    this.radius = 100,
  });

  @override
  Widget build(BuildContext context) {
    var conf = configuration;
    conf ??= PomodoroConfiguration();

    return PomodoroView(
      configuration: conf,
      onPomodoroWorkStart: onPomodoroWorkStarted,
      onPomodoroWorkFinished: onPomodoroWorkFinished,
      onPomodoroBreakStart: onPomodoroBreakStart,
      onPomodoroBreakEnd: onPomodoroBreakEnd,
      onAllPomodoriCompleted: onAllPomodoriCompleted,
      onPomodoroLongBreakStart: onPomodoroLongBreakStart,
      onPomodoroLongBreakEnd: onPomodoroLongBreakEnd,
      onCompleted: onCompleted,
      onStopped: onStopped,
      onStatusChanged: onStatusChanged,
      radius: radius,
    );
  }
}
