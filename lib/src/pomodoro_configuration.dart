import 'package:flutter/material.dart';

class PomodoroConfiguration {
  final int workingMinutes;
  final int breakMinutes;
  final int longerBreakMinutes;
  final int countUntilLongerBreak;
  final int totalPomodori;
  final Color workingColor;
  final Color workingShadowColor;
  final Color breakColor;
  final Color breakShadowColor;
  final Color longerBreakColor;
  final Color longerBreakShadowColor;
  final Color finishColor;
  final Color finishShadowColor;
  final bool autoStart; // Start Pomodoro when opening.

  PomodoroConfiguration({
    this.workingMinutes = 25,
    this.breakMinutes = 5,
    this.longerBreakMinutes = 15,
    this.countUntilLongerBreak = 4,
    this.totalPomodori = 3,
    this.workingColor = Colors.green,
    this.workingShadowColor = const Color.fromARGB(130, 15, 125, 58),
    this.breakColor = Colors.yellow,
    this.breakShadowColor = const Color.fromARGB(130, 225, 157, 21),
    this.longerBreakColor = Colors.orange,
    this.longerBreakShadowColor = const Color.fromARGB(161, 250, 142, 0),
    this.finishColor = Colors.blue,
    this.finishShadowColor = const Color.fromARGB(130, 0, 113, 243),
    this.autoStart = false,
  });
}
