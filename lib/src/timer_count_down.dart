import 'dart:async';
import 'package:flutter/widgets.dart';
import 'timer_controller.dart';

class Countdown extends StatefulWidget {
  final Widget Function(BuildContext, double) build;

  final Function? onStart;
  final Function? onReset;
  final Function? onFinished;
  final Function? onCompleted;

  // Build interval
  final Duration interval;

  final CountdownController controller;

  const Countdown({
    super.key,
    required this.build,
    this.interval = const Duration(seconds: 1),
    this.onStart,
    this.onReset,
    this.onFinished,
    this.onCompleted,
    required this.controller,
  });

  @override
  State<Countdown> createState() => _CountdownState();
}

class _CountdownState extends State<Countdown> {
  // Multiplier of seconds
  final int _secondsFactor = 1000000;
  Timer _timer = Timer(const Duration(seconds: 0), () {});
  late int _currentMicroSeconds;
  CountdownController get _controller => widget.controller;

  @override
  void initState() {
    _currentMicroSeconds =
        widget.controller.countTimeInSeconds * _secondsFactor;

    widget.controller.setOnStart(_onTimerStart);
    widget.controller.setOnReset(_onTimerReset);
    widget.controller.setMarkCompleted(_markCompleted);
    widget.controller.setMarkStop(_markStop);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.build(context, _currentMicroSeconds / _secondsFactor);
  }

  @override
  void dispose() {
    if (_timer.isActive == true) {
      _timer.cancel();
    }
    super.dispose();
  }

  void _onTimerReset() {
    setState(() =>
        _currentMicroSeconds = _controller.countTimeInSeconds * _secondsFactor);
    _startTimer();
  }

  void _onTimerStart() {
    widget.onStart?.call();
    setState(() =>
        _currentMicroSeconds = _controller.countTimeInSeconds * _secondsFactor);
    _startTimer();
  }

  void _markStop() {
    debugPrint("markStop called");
    _timer.cancel();
    setState(() =>
        _currentMicroSeconds = _controller.countTimeInSeconds * _secondsFactor);
  }

  void _markCompleted() {
    _timer.cancel();
    widget.onCompleted?.call();
    setState(() =>
        _currentMicroSeconds = _controller.countTimeInSeconds * _secondsFactor);
  }

  void _onTimerCompleted() {
    _timer.cancel();
    widget.onFinished?.call();
    setState(() =>
        _currentMicroSeconds = _controller.countTimeInSeconds * _secondsFactor);
  }

  void _startTimer() {
    if (_timer.isActive == true) {
      _timer.cancel();
    }

    if (_currentMicroSeconds != 0) {
      _timer = Timer.periodic(
        widget.interval,
        (Timer timer) {
          if (_currentMicroSeconds == 0) {
            _onTimerCompleted();
          } else {
            setState(() {
              _currentMicroSeconds =
                  _currentMicroSeconds - widget.interval.inMicroseconds;
            });
          }
        },
      );
    }
  }
}
