enum PomodoroStatus {
  none, working, shortBreak, longBreak, stopped, finished
}