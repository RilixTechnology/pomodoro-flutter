import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'circular_progress_timer.dart';
import 'pomodoro_configuration.dart';
import 'pomodoro_status.dart';
import 'progress_timer_controller.dart';
import 'package:wakelock_plus/wakelock_plus.dart';

class PomodoroView extends StatefulWidget {
  final PomodoroConfiguration configuration;
  final VoidCallback? onPomodoroWorkStart;
  final VoidCallback? onPomodoroWorkFinished; // on single pomodoro is finished.
  final VoidCallback? onPomodoroBreakStart;
  final VoidCallback? onPomodoroBreakEnd;
  final VoidCallback? onPomodoroLongBreakStart;
  final VoidCallback? onPomodoroLongBreakEnd;
  final Function(PomodoroStatus status)? onStatusChanged;

  // Total pomodori from config is achieved.
  final VoidCallback? onAllPomodoriCompleted;
  final VoidCallback? onCompleted;
  final VoidCallback? onStopped;
  final double radius;

  const PomodoroView({
    super.key,
    required this.configuration,
    this.onCompleted,
    this.onPomodoroWorkStart,
    this.onPomodoroWorkFinished,
    this.onPomodoroBreakStart,
    this.onPomodoroBreakEnd,
    this.onAllPomodoriCompleted,
    this.onPomodoroLongBreakStart,
    this.onPomodoroLongBreakEnd,
    this.onStopped,
    this.onStatusChanged,
    this.radius = 150,
  });

  @override
  PomodoroViewState createState() => PomodoroViewState();
}

class PomodoroViewState extends State<PomodoroView> {
  final _controller = ProgressTimerController();
  late AudioPool _watchTickAudio;
  late AudioPool _taskFinishedSound;

  PomodoroConfiguration get _config => widget.configuration;

  int _totalPomodoroUntilLongBreak = 0;
  int _totalPomodoro = 0;

  PomodoroStatus _currentProgressStatus = PomodoroStatus.none;

  void setNextPomodoroState(PomodoroStatus currentStatus) {
    debugPrint("currentStatus: $currentStatus");
    switch (currentStatus) {
      case PomodoroStatus.none:
        _currentProgressStatus = PomodoroStatus.working;
        _controller.setCountTimeInMinutes(
          _getMinuteDurationForState(_currentProgressStatus),
        );
        _controller.setProgressColor(_config.workingColor);
        _controller.setProgressShadowColor(_config.workingShadowColor);
        _controller.start();
        _totalPomodoroUntilLongBreak = 1;
        _totalPomodoro++;
        widget.onPomodoroWorkStart?.call();
        break;
      case PomodoroStatus.working:
        widget.onPomodoroWorkFinished?.call();
        if (_totalPomodoro == _config.totalPomodori) {
          _currentProgressStatus = PomodoroStatus.none;
          _controller.setProgressColor(_config.finishColor);
          _controller.setProgressShadowColor(_config.finishShadowColor);
          _controller.stop();
          _totalPomodoro = 0;
          _totalPomodoroUntilLongBreak = 0;
          widget.onAllPomodoriCompleted?.call();
        } else {
          debugPrint(
              "_totalPomodoroUntilLongBreak: $_totalPomodoroUntilLongBreak");
          debugPrint(
              "_config.countUntilLongerBreak: ${_config.countUntilLongerBreak}");

          if (_totalPomodoroUntilLongBreak == _config.countUntilLongerBreak) {
            _currentProgressStatus = PomodoroStatus.longBreak;
            _controller.setProgressColor(_config.longerBreakColor);
            _controller.setProgressShadowColor(_config.longerBreakShadowColor);
            _totalPomodoroUntilLongBreak = 0;
            widget.onPomodoroLongBreakStart?.call();
          } else {
            _currentProgressStatus = PomodoroStatus.shortBreak;
            _controller.setProgressColor(_config.breakColor);
            _controller.setProgressShadowColor(_config.breakShadowColor);
            widget.onPomodoroBreakStart?.call();
            debugPrint("is on break");
          }
          _controller.setCountTimeInMinutes(
            _getMinuteDurationForState(_currentProgressStatus),
          );
          _controller.restart();
          widget.onPomodoroWorkStart?.call();
        }
        break;
      case PomodoroStatus.shortBreak:
        widget.onPomodoroBreakEnd?.call();
        _currentProgressStatus = PomodoroStatus.working;
        _totalPomodoroUntilLongBreak += 1;
        _totalPomodoro++;
        _controller.setCountTimeInMinutes(
          _getMinuteDurationForState(_currentProgressStatus),
        );
        _controller.setProgressColor(_config.workingColor);
        _controller.setProgressShadowColor(_config.workingShadowColor);
        _controller.restart();
        widget.onPomodoroWorkStart?.call();
        break;
      case PomodoroStatus.longBreak:
        widget.onPomodoroLongBreakEnd?.call();
        _currentProgressStatus = PomodoroStatus.working;
        _totalPomodoro++;
        _controller.setCountTimeInMinutes(
          _getMinuteDurationForState(_currentProgressStatus),
        );
        _controller.setProgressColor(_config.workingColor);
        _controller.setProgressShadowColor(_config.workingShadowColor);
        _controller.restart();
        widget.onPomodoroWorkStart?.call();
        break;
      case PomodoroStatus.finished:
        _currentProgressStatus = PomodoroStatus.none;
        _controller.setProgressColor(_config.finishColor);
        _controller.setProgressShadowColor(_config.finishShadowColor);
        _controller.finish();
        break;
      case PomodoroStatus.stopped:
        // If pomodoro is stop, don't allow to go to working state
        // unless it it started from tap/click.
        _currentProgressStatus = PomodoroStatus.none;
        _controller.setProgressColor(_config.workingColor);
        _controller.setProgressShadowColor(_config.workingShadowColor);
        _controller.stop();
        break;
    }
    widget.onStatusChanged?.call(_currentProgressStatus);
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.endOfFrame.then((_) {
      _afterFirstLayout(context);
    });
  }

  void _afterFirstLayout(BuildContext context) async {
    await _setupSounds();
    _controller.setCountTimeInMinutes(
      _getMinuteDurationForState(_currentProgressStatus),
    );
    WakelockPlus.enable();
    _startOrReset();
  }

  @override
  void dispose() {
    WakelockPlus.disable();
    super.dispose();
  }

  Future _setupSounds() async {
    var watchTickCache = AudioCache(prefix: "");
    var taskFinishCache = AudioCache(prefix: "");

    _watchTickAudio = await AudioPool.createFromAsset(
      path: "packages/pomodoro_flutter/assets/sounds/watch_tick.wav",
      maxPlayers: 1,
      audioCache: watchTickCache,
    );
    _taskFinishedSound = await AudioPool.createFromAsset(
      path: "packages/pomodoro_flutter/assets/sounds/alarm_clock.mp3",
      maxPlayers: 1,
      audioCache: taskFinishCache,
    );
    debugPrint("finish setup sound");
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
        onTap: _startOrReset,
        child: CircularProgressTimer(
          radius: widget.radius,
          controller: _controller,
          onFinished: () async {
            debugPrint("onFinished");
            await _taskFinishedSound.start();
            //TODO: Notification
            // MyNotificationManager.instance.showNotificationCustomVibrationIconLed();
            setNextPomodoroState(_currentProgressStatus);
          },
          onSecondTick: () async {
            await _watchTickAudio.start();
          },
          onStart: () {},
          onReset: () {},
          onCompleted: () {
            widget.onCompleted?.call();
          },
        ),
      ),
    );
  }

  int _getMinuteDurationForState(PomodoroStatus currentStatus) {
    switch (currentStatus) {
      case PomodoroStatus.none:
        return _config.workingMinutes;
      case PomodoroStatus.working:
        return _config.workingMinutes;
      case PomodoroStatus.shortBreak:
        return _config.breakMinutes;
      case PomodoroStatus.longBreak:
        return _config.longerBreakMinutes;
      case PomodoroStatus.stopped:
        return _config.workingMinutes;
      case PomodoroStatus.finished:
        return _config.workingMinutes;
    }
  }

  void _startOrReset() {
    debugPrint("status: $_currentProgressStatus");
    switch (_currentProgressStatus) {
      case PomodoroStatus.none:
      case PomodoroStatus.stopped:
      case PomodoroStatus.finished:
        _currentProgressStatus = PomodoroStatus.none;
        setNextPomodoroState(_currentProgressStatus);
        break;
      case PomodoroStatus.working:
      case PomodoroStatus.shortBreak:
      case PomodoroStatus.longBreak:
        _confirmToStop();
    }
  }

  void _confirmToStop() {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();
    var bar = SnackBar(
      content: const Text('Confirm to cancel.'),
      action: SnackBarAction(
        onPressed: () {
          _currentProgressStatus = PomodoroStatus.none;
          _controller.stop();
          widget.onStopped?.call();
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
          setState(() {});
        },
        label: 'Confirm',
      ),
      duration: const Duration(seconds: 7),
    );
    ScaffoldMessenger.of(context).showSnackBar(bar);
  }
}
