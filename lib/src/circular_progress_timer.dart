import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

import 'progress_timer_controller.dart';
import 'timer_controller.dart';
import 'timer_count_down.dart';

class CircularProgressTimer extends StatefulWidget {
  final ProgressTimerController controller;
  final VoidCallback onFinished;
  final VoidCallback onCompleted;
  final Function onSecondTick;
  final Function? onStart;
  final Function? onReset;
  final double radius;

  const CircularProgressTimer({
    super.key,
    required this.controller,
    required this.onSecondTick,
    required this.onFinished,
    required this.onCompleted,
    required this.radius,
    this.onStart,
    this.onReset,
  });

  @override
  State<CircularProgressTimer> createState() => _CircularProgressTimerState();
}

class _CircularProgressTimerState extends State<CircularProgressTimer>
    with TickerProviderStateMixin {
  static const int _pulseDuration = 2000; // milliseconds.
  int _currentPulseDuration = _pulseDuration;

  final int _timerInterval = 100; // milliseconds
  int _currentInterval = 0;

  CountdownController get controller => widget.controller.controller;
  late AnimationController _animationController;
  Animation? _animation;

  Color _shadowColor = const Color.fromARGB(130, 15, 125, 58);

  Color _progressColor = Colors.blueAccent;

  void _setColor() {
    _shadowColor = controller.progressShadowColor;
    _progressColor = controller.progressColor;
    debugPrint("_shadowColor: $_progressColor");
  }

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: _currentPulseDuration),
    );
    _animation = Tween(
      begin: 2.0,
      end: 15.0,
    ).animate(_animationController);
    controller.addListener(() {
      _setColor();
    });
  }

  @override
  void dispose() {
    _animationController.dispose();
    if (_animation != null) _animation = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Countdown(
      controller: controller,
      build: (ctx, time) {
        _currentInterval += _timerInterval;
        if (_currentInterval % 1000 == 0) {
          widget.onSecondTick();
        }
        return _progressIndicator(
          controller.countTimeInSeconds.toDouble(),
          time,
        );
      },
      interval: Duration(milliseconds: _timerInterval),
      onFinished: () {
        debugPrint("circular progress timer onFinished");
        widget.onFinished();

        if (controller.condition == TimerCondition.stop) {
          _animationController.stop();
        } else {
          _animationController.repeat(reverse: true);
          debugPrint("repeat");
        }
        _setColor();
      },
      onStart: () {
        debugPrint("circular progress timer onStart");
        _currentInterval = 0;
        _animationController.repeat(reverse: true);
        widget.onStart?.call();
        _setColor();
      },
      onReset: () {
        debugPrint("circular progress timer onReset");
        _currentInterval = 0;
        _animationController.reset();
        widget.onReset?.call();
        _setColor();
      },
      onCompleted: () {
        debugPrint("completed");
        widget.onCompleted();
        _setColor();
      },
    );
  }

  Widget _progressIndicator(double total, double time) {
    var progress = (_pulseDuration - time) / _pulseDuration; // in percentage.
    var curr = (_pulseDuration * progress).toInt();
    _currentPulseDuration = _pulseDuration - curr;
    return _circularIndicator(total, time);
  }

  Widget _circularIndicator(double total, double secondTime) {
    return Container(
      decoration: _pulseBoxDecoration(),
      child: CircularPercentIndicator(
        radius: widget.radius,
        lineWidth: 20.0,
        percent: (total - secondTime) / total,
        center: _minuteSecondWidget(secondTime),
        progressColor: _progressColor,
      ),
    );
  }

  Widget _minuteSecondWidget(double secondTime) {
    int minute = secondTime.toInt() ~/ 60;
    int second = secondTime.toInt() % 60;

    return Text(
      "$minute:${second < 10 ? '0$second' : second}",
      style: const TextStyle(
        fontSize: 42,
        fontWeight: FontWeight.w600,
      ),
    );
  }

  BoxDecoration _pulseBoxDecoration() {
    return BoxDecoration(
      shape: BoxShape.circle,
      color: Colors.white,
      boxShadow: [
        BoxShadow(
          color: _shadowColor,
          blurRadius: _animation!.value,
          spreadRadius: _animation!.value,
        )
      ],
    );
  }
}
