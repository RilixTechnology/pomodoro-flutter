import 'package:flutter/material.dart';
import 'timer_controller.dart';

class ProgressTimerController {
  CountdownController controller = CountdownController();

  void setCountTimeInMinutes(int minutes) {
    controller.setCountTimeInMinutes(minutes);
  }

  void setProgressColor(Color color) {
    debugPrint("setProgressColor is called");
    controller.setProgressColor(color);
  }

  void setProgressShadowColor(Color color) {
    controller.setProgressShadowColor(color);
  }

  void start() {
    controller.start();
  }

  void stop() {
    controller.stop();
  }

  void restart() {
    controller.reset();
  }

  void finish() {
    controller.setCompleted();
  }
}
