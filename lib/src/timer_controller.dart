import 'package:flutter/material.dart';

enum TimerCondition {
  none, run, stop, reset, complete
}

class CountdownController extends ChangeNotifier {
  VoidCallback? onReset;
  VoidCallback? markStop;
  VoidCallback? onStart;
  VoidCallback? markCompleted;

  TimerCondition condition = TimerCondition.none;
  int _countTimeInSeconds = 25 * 60;
  Color _progressColor = Colors.green;
  Color _progressShadowColor = const Color.fromARGB(130, 15, 125, 58);

  CountdownController();

  int get countTimeInSeconds => _countTimeInSeconds;

  setCountTimeInMinutes(int timeInMinutes) {
    _countTimeInSeconds = timeInMinutes * 60;
  }

  Color get progressColor => _progressColor;

  setProgressColor(Color color) {
    _progressColor = color;
    notifyListeners();
  }

  Color get progressShadowColor => _progressShadowColor;

  setProgressShadowColor(Color color) {
    _progressShadowColor = color;
    notifyListeners();
  }

  setCompleted() {
    condition = TimerCondition.complete;
    markCompleted?.call();
  }

  setMarkCompleted(VoidCallback? markCompleted) {
    this.markCompleted = markCompleted;
  }

  // Start the counter.
  start() {
    condition = TimerCondition.run;
    onStart?.call();
  }

  setOnStart(VoidCallback? onStart) {
    this.onStart = onStart;
  }

  stop() {
    condition = TimerCondition.stop;
    markStop?.call();
  }

  setMarkStop(VoidCallback markStop) {
    this.markStop = markStop;
  }

  reset () {
    condition = TimerCondition.reset;
    onReset?.call();
  }

  setOnReset(VoidCallback? onReset) {
    this.onReset = onReset;
  }
}
